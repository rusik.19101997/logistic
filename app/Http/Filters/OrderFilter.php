<?php

namespace App\Http\Filters;

use Illuminate\Database\Eloquent\Builder;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class OrderFilter extends BaseFilter
{
    protected $filters = [ 'sort', 'store', 'price', 'delivery_address', 'delivery_date' ];

    protected function price ( $name ): Builder {
        return $this->builder->where( 'name', 'ilike', "{$name}%" );
    }

    protected function deliveryAddress ( $name ): Builder {
        return $this->builder->whereJsonContains( 'place_to', 'like', [ 'name' => "%$name%" ] );
    }

    protected function deliveryDate ( $name ): Builder {
        return $this->builder->whereJsonContains( 'place_to', 'like', [ 'name' => "%$name%" ] );
    }

    protected function sort ( $sorts ): Builder {
        foreach ( $sorts as $sort ) {
            [ $column, $direction ] = json_decode( $sort, true );

            if ( !is_bool( $direction ) ) {
                throw new BadRequestHttpException( "Bad direction for sorting" );
            }

            try {
                $this->builder->orderBy( $column, $direction ? 'desc' : 'asc' );
            } catch ( \Exception $exception ) {
                throw new BadRequestHttpException( "Bad direction for sorting" );
            }
        }

        return $this->builder;
    }
}
