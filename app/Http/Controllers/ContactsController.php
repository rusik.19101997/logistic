<?php

namespace App\Http\Controllers;

use App\Models\Setting;

class ContactsController
{
    public function index() {
        return view('contacts', [ 'contacts' => Setting::query()->where( "key", "contacts" )->first() ]);
    }
}
