<?php

namespace App\Http\Controllers;

use App\Models\Store;
use App\Services\MapService;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpKernel\Exception\HttpException;

class MapPointsController extends Controller
{
    public function stores () {
        return Store::query()->where( 'is_active', true )->get();
    }

    public function getAllowedStore ( Request $request ) {
        [ $latitude, $longitude ] = $request->input( 'coordinates' );

        $service = new MapService();

        if ( $store = $service->getAllowedStore( $latitude, $longitude ) ) {
            return $store;
        }

        throw new HttpException(400);
    }
}
