<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = Setting::query()->whereIn('key', ['slider', 'content'])->get();
        $content = $data->where('key', 'content')->first()->value;
        $main_banners = $data->where('key', 'slider')->first()->value;

        return view('home', ['content' => $content['data'], 'main_banners' => $main_banners] );
    }
}
