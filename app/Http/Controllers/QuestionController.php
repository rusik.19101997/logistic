<?php

namespace App\Http\Controllers;

use App\Events\QuestionCreated;
use App\Http\Requests\QuestionFormRequest;
use App\Models\Question;

class QuestionController
{
    public function store(QuestionFormRequest $request) {
        $question = Question::query()->create($request->validated());
        QuestionCreated::dispatch($question);

        return response()->json([
            'success' => true
        ]);
    }
}

