<?php

namespace App\Http\Controllers;

use App\Models\Setting;

class AboutController extends Controller
{
    public function index () {
        return view( 'about', [ 'content' => Setting::query()->firstOrCreate( [ 'key' => 'about' ], [ 'value' => [ 'data' => '' ] ] )->value['data'] ] );
    }
}
