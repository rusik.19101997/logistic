<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\SmsAuthenticationRequest;
use App\Http\Requests\SmsConfirmRequest;
use App\Models\User;
use App\Notifications\SmsNotification;
use App\Providers\RouteServiceProvider;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct () {
        $this->middleware( 'guest' )->except( 'logout' );
    }

    public function login ( SmsAuthenticationRequest $request ) {
        $user = User::query()->where( [
            'phone' => preg_replace( '/[^\d]/', '', $request->get( 'phone' ) )
        ] )->first();

        if($user) {
            $user->generateAuthCodeAndSend();

            return redirect( "/confirm-code?phone={$user->phone}");
        }

        return view('auth.login')->withErrors(['phone' => 'Пользователь не найден']);
    }

    public function confirmCode ( SmsAuthenticationRequest $request ) {
        $user = User::query()->where( [
            'phone' => preg_replace( '/[^\d]/', '', $request->get( 'phone' ) )
        ] )->firstOrFail();


        return view( 'auth.code_confirm', [ 'user_id' => $user->id ] );
    }

    public function confirmCodeAndLogin ( SmsConfirmRequest $request ) {
        $user = User::query()->findOrFail( $request->get( 'user_id' ) );
        $code =  str_replace(' ', '', $request->get( 'code' ));

        if ( $user->verifyAuthCode( (int) $code ) && Auth::loginUsingId( $user->id, true ) ) {
            session()->remove('has_order');
            return redirect( '/' );
        }

        return view( 'auth.code_confirm', [ 'user_id' => $user->id ] )->withErrors( [ "code" => "Code is invalid" ] );
    }

    /**
     * @param Request $request
     */
    public function resendCode ( Request $request ) {
        /** @var User $user */
        $user = User::query()->findOrFail( $request->input( 'user_id' ) );
        $date = now()->addMinute();

        if ( $time = Cache::get( "resend_code_{$user->id}" ) ) {
            return [ 'data' => now()->diffInSeconds( $time ) ];
        }

        $user->generateAuthCodeAndSend();
        $time = $date->toDateTimeString();

        Cache::put( "resend_code_{$user->id}", $time, $date );

        return [ 'data' => now()->diffInSeconds( $time ) ];
    }
}
