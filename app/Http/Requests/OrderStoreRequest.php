<?php

namespace App\Http\Requests;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class OrderStoreRequest extends FormRequest
{
    public function rules () {
        $rules = [
            'price'  => 'numeric|required',
            'detail' => 'array|required',
            'shipping_date' => 'required'
        ];

        $points = ( new MapPointsRequest )->rules();

        return array_merge( $points, $rules );
    }
}
