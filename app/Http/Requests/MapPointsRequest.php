<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MapPointsRequest extends FormRequest
{
    public function rules() {
        return [
            'store_id' => 'required|exists:stores,id',
            'place_to.name' => 'required',
            'place_to.latitude' => 'required',
            'place_to.longitude' => 'required',
        ];
    }
}
