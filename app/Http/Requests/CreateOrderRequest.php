<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use \Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class CreateOrderRequest extends FormRequest
{
    protected $redirect = '/';
    public function authorize() {
        return true;
    }

    protected function prepareForValidation()
    {
        $this->merge([ 'from' => json_decode($this->from, true), 'to' => json_decode($this->to, true)]);
    }

    public function rules() {
        return [
            'from' => 'required|array',
            'from.coordinates' => 'required_with:from|required|size:2|store_exist',
            'from.text' => 'required_with:from|required',
            'to' => 'required|array',
            'to.coordinates' => 'required_with:from|required|size:2',
            'to.text' => 'required_with:from|required',
        ];

      /*  $rules = [];
        if ($this->from) {
            $rules = array_merge($rules, [
                'from' => 'array',
                'from.coordinates' => 'required_with:from|required|size:2|store_exist',
                'from.text' => 'required_with:from|required',
            ]);
        }

        if ($this->to) {
            $rules = array_merge($rules, [
                'to' => 'array',
                'to.coordinates' => 'required_with:from|required|size:2',
                'to.text' => 'required_with:from|required',
            ]);
        }

        return $rules*/;
    }

    protected function failedValidation(Validator $validator) {
        $this->session()->flash('first-step-error', 'Произошла ошибка подбора координат. Пожалуйста повторите еще раз');
        parent::failedValidation($validator);
    }

}
