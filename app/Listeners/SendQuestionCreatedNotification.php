<?php

namespace App\Listeners;

use App\Events\QuestionCreated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;
use  App\Mail\QuestionCreated as Created;

class SendQuestionCreatedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\QuestionCreated  $event
     * @return void
     */
    public function handle(QuestionCreated $event)
    {
        $question = $event->question;

        Mail::to(config('mail.log_email'))->locale('es')->send(
            new Created($question)
        );
    }
}
