<?php

namespace App\Providers;

use App\Models\Setting;
use App\View\Components\FirstOrderForm;
use App\View\Components\QuestionForm;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot () {
        Blade::component( QuestionForm::class, 'question-form' );
        Blade::component( FirstOrderForm::class, 'first-order-form' );

        View::share(
            'contacts_data',
            Setting::query()->firstOrCreate( [ 'key' => 'basic' ], [ 'value' => ['phone' => '', 'email'=> '', 'schedule' => ''] ] )->value
        );
    }
}
