<?php

namespace App\Providers;

use App\Models\Setting;
use App\Models\Store;
use App\View\Components\FirstOrderForm;
use App\View\Components\QuestionForm;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ValidatorServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot () {
        $this->app[ 'validator' ]->extend( 'store_exist', fn ( $attribute, $value, $parameters ) => $this->storeExist($value) );
    }

    public function storeExist($value) {
        [ $latitube, $longitube ] = $value;

        return Store::query()->where(
            [
                'longitude' => $longitube,
                'latitude'  => $latitube,
            ]
        )->orWhere(
            [
                'longitude' => $latitube,
                'latitude'  => $longitube,
            ]
        )->exists();
    }
}

