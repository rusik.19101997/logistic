<?php

namespace App\Services\Clients;

abstract class ApiClient
{
    abstract protected function getUrl ();

    abstract protected function getConfig ();

    abstract protected function parseData ();

    public function execute () {
    }
}
