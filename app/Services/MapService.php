<?php

namespace App\Services;

use App\Models\Store;

class MapService
{
    const POLYGONS = [];

    public  function isAllowedPoints(): bool {
        return true;
    }

    public function getAllowedStore ( $latitude, $longitude ) {
        return Store::nearlyPoint($latitude, $longitude)->active()->with('polygons')->first();
    }
}
