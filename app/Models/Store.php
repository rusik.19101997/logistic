<?php

namespace App\Models;

use Encore\Admin\Form\Field\HasMany;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use MStaack\LaravelPostgis\Eloquent\PostgisTrait;

/**
 * @property $name
 * @property $latitude
 * @property HasMany | StorePolygon [] $polygons
 * @property $longitude@method static Builder|\App\Models\User
 * @method static Builder| self nearlyPoint( $latitude, $longitude )
 * @method static Builder| self active()
 */
class Store extends Model
{
    use HasFactory;

    protected $fillable = [
        'longitude',
        'latitude',
        'name',
        'is_active'
    ];

    public function scopeNearlyPoint ( Builder $builder, $latitude, $longitude ): Builder {
        return $builder->whereHas('polygons', function (Builder $query) use ($latitude, $longitude) {
            return $query->whereRaw("ST_Intersects(area, 'SRID=4326;POINT($latitude $longitude)'::geography)");
        });
    }

    public function scopeActive ( Builder $builder ) {
        return $builder->whereIsActive( true );
    }

    public function orders() {
        return $this->hasMany(Order::class);
    }

    public function polygons() {
        return $this->hasMany(StorePolygon::class);
    }
}
