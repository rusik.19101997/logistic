<?php

namespace App\Models;

use App\Traits\SmsAuth;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

/**
 * @property integer $id
 * @property string $phone
 * @property string $name
 * @property string $email
 * @property string $password
 * @method static Builder|\App\Models\User FindByPhone( $phone)

 */
class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, SmsAuth;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'phone',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function scopeFindByPhone ( Builder $builder,  $phone ) {
        return $builder->where( 'phone', $phone )->first();
    }
}
