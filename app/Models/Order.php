<?php

namespace App\Models;

use App\Traits\HasFilter;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property Store $store
 */
class Order extends Model
{
    use HasFactory, HasFilter;

    const STATUS_APPLICATION_COMPLETED = 'application_completed';
    const STATUS_REQUIRES_ACTION       = 'requires_action';
    const CANCELED                     = 'canceled';
    const DELIVERED                    = 'delivered';

    const STATUSES = [
        self::STATUS_APPLICATION_COMPLETED => 'Заявка оформлена',
        self::STATUS_REQUIRES_ACTION       => 'Требует действия',
        self::CANCELED                     => 'Отменен',
        self::DELIVERED                    => 'Доставлен',

    ];

    protected $dates = [
        'delivery_date',
        'shipping_date'
    ];

    protected $fillable = [
        'user_id',
        'store_id',
        'place_to',
        'place_to.name',
        'place_to.latitude',
        'place_to.longitude',
        'price',
        'detail',
        'delivery_date',
        'shipping_date'
    ];

    protected $casts = [
        'place_to' => 'json',
        'detail' => 'json',
    ];
    protected $appends = [
        'store_name'
    ];

    public function getStatusNameAttribute ( $key ): string {
        return self::STATUSES[ $key ];
    }

    public function store () {
        return $this->belongsTo( Store::class,);
    }

    public function user () {
        return $this->belongsTo( User::class );
    }

    public function getStoreNameAttribute () {
        return $this->store->name;
    }

    public function getDeliveryDateAttribute ($attr) {
        return Carbon::parse($attr)->toDateString();
    }

    public function getShippingDateAttribute ($attr) {
        return Carbon::parse($attr)->toDateString();
    }

    public function getCreatedAtAttribute ($attr) {
        return Carbon::parse($attr)->toDateTimeString();
    }
}
