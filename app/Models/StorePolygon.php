<?php

namespace App\Models;

use GeoJson\Geometry\Polygon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use MStaack\LaravelPostgis\Eloquent\PostgisTrait;

/**
 * @property Polygon $area
 */
class StorePolygon extends Model
{
    use HasFactory, PostgisTrait;

    protected $fillable = [
        'store_id',
        'area'
    ];

    protected $postgisFields = [
        'area',
    ];

    protected $postgisTypes = [
        'area' => [
            'geomtype' => 'geography',
            'srid' => 4326
        ],
    ];

    public $timestamps = false;

}
