<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property $value
 */
class Setting extends Model
{
    use HasFactory;

    protected $fillable = [
        'value',
        'key',
        'name',
        'description',
    ];

    protected $casts = [
        'value' =>'json',
    ];
}
