<?php

namespace App\Admin\Controllers;

use App\Admin\Forms\About;
use App\Admin\Forms\Basic;
use App\Admin\Forms\Content as ContentForm;
use App\Admin\Forms\Slider;
use App\Http\Controllers\Controller;
use Encore\Admin\Layout\Content;
use Encore\Admin\Widgets\Tab;

class SettingController extends Controller
{
    public function index(Content $content): Content {
        $forms = [
            'content'    => ContentForm::class,
            'slider'  => Slider::class,
            'basic'    => Basic::class,
            'about'    => About::class,

        ];

        return $content
            ->title('Настройки')
            ->body(Tab::forms($forms));
    }
}
