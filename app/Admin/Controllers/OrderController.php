<?php

namespace App\Admin\Controllers;

use App\Models\Order;
use App\Models\Store;
use App\Models\User;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Encore\Admin\Widgets\Table;
use Illuminate\Support\Str;

class OrderController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Заказы';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid () {
        $grid = new Grid( new Order() );
        $grid->disableCreateButton();
        $grid->column( 'id', __( 'Номер' ) );
        $grid->column( 'user.name', __( 'Данные заказчика' ) )
             ->modal( 'Данные заказчика', function ( $order ) {
                 $user = collect( $order->user->toArray() )->except( 'email_verified_at', 'updated_at' )->toArray();

                 return new Table( [], $user );
             } );
        $grid->column( 'store.name', 'Склад' );
        $grid->column( 'shipping_date', 'Дата Отправки' );
        $grid->column( 'delivery_date', 'Дата доставки' );
        $grid->column( 'status', 'Статус' )->using(Order::STATUSES);
        $grid->column( 'price', 'Стоимость' );
        $grid->column( 'created_at', __( 'Дата заказа' ) );

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail ( $id ) {
        $show = new Show( Order::findOrFail( $id ) );

        $show->field( 'id', __( 'Номер' ) );
        $show->field( 'status', __( 'Номер' ) );
        $show->field( 'created_at', __( 'Created at' ) );
        $show->field( 'updated_at', __( 'Updated at' ) );

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form () {
        $params = [];
        Store::active()->each( function ( Store $item ) use(&$params) {
            $params[ $item->id ] = $item->name;
        } );

        $form = new Form( new Order() );

        $form->date( 'shipping_date', 'Дата Отправки' )->required();
        $form->date( 'delivery_date', 'Дата доставки' )->required();
        $form->number( 'price', 'Стоимость' )->required();

        $form->select( 'store_id', 'Склад' )->options( $params )->required();
        $form->select( 'status', 'статус' )->options( Order::STATUSES )->required();

        return $form;
    }
}
