<?php

namespace App\Admin\Controllers;

use App\Models\Question;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Encore\Admin\Widgets\Table;
use Illuminate\Support\Str;

class QuestionController extends AdminController
{

    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Question';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid () {
        $grid = new Grid( new Question() );

        $grid->disableCreateButton();
        $grid->column( 'id', __( 'Id' ) );
        $grid->column( 'name', __( 'Название' ) );

        $grid->column( 'text', __( 'Текст' ) )->display(function ($text) {
            return Str::limit($text, 50);
        })->modal( 'Текст', function ( $question ) {
            return new Table( [ ], $question->toArray() );
        } );

        $grid->column( 'phone', __( 'Телефон' ) );
        $grid->column( 'created_at', __( 'Дата создания' ) );
//        $grid->actions( function ( $actions ) {
//            $actions->disableDelete();
//            $actions->disableEdit();
//        } );

        $grid->disableActions();


        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail ( $id ) {
        $show = new Show( Question::findOrFail( $id ) );

        $show->field( 'id', __( 'Id' ) );
        $show->field( 'name', __( 'Имя' ) );
        $show->field( 'text', __( 'Текст' ) );
        $show->field( 'phone', __( 'Телефон' ) );
        $show->field( 'created_at', __( 'Дата создания' ) );

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form () {
        $form = new Form( new Question() );

        $form->text( 'name', __( 'Имя' ) );
        $form->text( 'text', __( 'Текст' ) );
        $form->mobile( 'phone', __( 'Телефон' ) );

        return $form;
    }
}
