<?php

namespace App\Admin\Controllers;

use App\Models\Setting;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Form\EmbeddedForm;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class ContactController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Setting';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid () {
        $grid = new Grid( new Setting() );

        $grid->column( 'id', __( 'Id' ) );
        //    $grid->column('value', __('Value'));
        $grid->column( 'key', __( 'Key' ) );
        $grid->column( 'name', __( 'Name' ) );
        $grid->column( 'description', __( 'Description' ) );

        return $grid;
    }

    public function index ( Content $content ) {
        $id = Setting::query()->firstOrCreate( ["key" => "contacts"], ['value' => []])->first()->id;

        return redirect( "admin/contacts/$id/edit" );
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail ( $id ) {
        $show = new Show( Setting::findOrFail( $id ) );

        $show->field( 'id', __( 'Id' ) );
        //  $show->field('value', __('Value'));
        $show->field( 'name', __( 'Name' ) );
        $show->field( 'description', __( 'Description' ) );

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form () {
        $form = new Form( new Setting() );

        $form->embeds('value', __("Форма контактов"), function (EmbeddedForm  $form) {
            $form->mobile('phone','Телефон')->rules('required');
            $form->email('email')->rules('required');
            $form->text('schedule','График работы')->rules('required');
        });
        $form->text( 'name', __( 'Name' ) );
        $form->text( 'description', __( 'Description' ) );

        return $form;
    }
}
