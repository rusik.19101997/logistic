<?php

namespace App\Admin\Controllers;

use App\Models\Setting;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Form\EmbeddedForm;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class MainPageController  extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Главная';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid () {
        $grid = new Grid( new Setting() );

        $grid->column( 'id', __( 'Id' ) );
        //    $grid->column('value', __('Value'));
        $grid->column( 'key', __( 'Key' ) );
        $grid->column( 'name', __( 'Name' ) );
        $grid->column( 'description', __( 'Description' ) );

        return $grid;
    }

    public function index ( Content $content ) {
        $id = Setting::query()->firstOrCreate( ["key" => "main"], ['value' => []])->first()->id;

        return redirect( "admin/main/$id/edit" );
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail ( $id ) {
        $show = new Show( Setting::findOrFail( $id ) );

        $show->field( 'id', __( 'Id' ) );
        $show->field('value', __('Value'));


        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form () {
        $form = new Form( new Setting() );

        $form->embeds('value', __("Контент"), function (EmbeddedForm  $form) {
            $form->ckeditor('about', 'О нас')->options(['lang' => 'ru', 'height' => 300]);

            $form->table('how_to_use_service','Баннеры на главной',function ($table) {
                $table->image('image','Картинка')->move(storage_path('admin/images'))->rules('required');
                $table->text('title', 'Заголовок')->rules('required');
                $table->text('text', 'Текст')->rules('required');
                $table->text('time', 'Срок')->rules('required');
                $table->text('price', 'Цена')->rules('required');
            })->rules('required|min:5');

        });

        return $form;
    }
}
