<?php

namespace App\Admin\Forms;

use App\Models\Setting;
use Encore\Admin\Widgets\Form;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;

class Slider extends Form
{
    /**
     * The form title.
     *
     * @var string
     */
    public $title = 'Баннеры';
    /**
     * @var \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    private $setting;

    public function __construct ( $data = [] ) {
        parent::__construct( $data );
        $this->setting = Setting::query()->firstOrCreate( [ 'key' => 'slider' ], [ 'value' => [] ] );
    }

    /**
     * Handle the form request.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function handle ( Request $request ) {
        $data = $request->all();
        foreach ( $data[ 'value' ] as $key => $item ) {
            /** @var UploadedFile $imageFromObj */
            $imageFromObj = $item[ 'image' ];
            $imageName    = time() . '.' . $imageFromObj->extension();
            $images_path = config('admin.images_path');

            $imageFromObj->move( public_path( $images_path ), $imageName );
            $data[ 'value' ][ $key ][ 'image' ] = "$images_path/$imageName";
        }

        $this->setting->update( $data );

        admin_success( 'Processed successfully.' );

        return redirect( '/admin/settings?active=slider' );
    }

    /**
     * Build a form here.
     */
    public function form () {
        $this->table( 'value', 'Баннеры на главной', function ( $table ) {
            $table->image( 'image', 'Картинка' )->move( storage_path( 'admin/images' ) )->rules( 'required' );
            $table->text( 'title', 'Заголовок' )->width(2000)->rules( 'required' );
            $table->text( 'text', 'Текст' )->rules( 'required' );
            $table->text( 'time', 'Срок' )->rules( 'required' );
            $table->text( 'price', 'Цена' )->rules( 'required' );
        } )->rules( 'required|min:3' );
    }

    public function data (): array {
        return $this->setting->toArray();
    }
}

