<?php

namespace App\Admin\Forms;

use App\Models\Setting;
use Encore\Admin\Form\EmbeddedForm;
use Encore\Admin\Widgets\Form;
use Illuminate\Http\Request;

class About extends Form
{
    /**
     * The form title.
     *
     * @var string
     */
    public $title = 'О сервисе';
    /**
     * @var \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    private $setting;

    public function __construct ( $data = [] ) {
        parent::__construct( $data );
        $this->setting = Setting::query()->firstOrCreate(['key' => 'about'], ['value' => ['data' => '']]);
    }

    /**
     * Handle the form request.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function handle ( Request $request ) {
        $this->setting->update($request->all());


        admin_success( 'Processed successfully.' );

        return redirect('/admin/settings?active=basic');
    }

    /**
     * Build a form here.
     */
    public function form () {
        $this->embeds('value', __("Форма контактов"), function (EmbeddedForm  $form) {
            $form->ckeditor('data', 'О нас')->options(['lang' => 'ru', 'height' => 800]);
        });
    }

    public function data()
    {
        return $this->setting->toArray();
    }
}
