<?php

namespace App\Admin\Forms;

use App\Models\Setting;
use Encore\Admin\Form\EmbeddedForm;
use Encore\Admin\Widgets\Form;
use Illuminate\Http\Request;

class Content extends Form
{

    protected $setting;

    /**
     * The form title.
     *
     * @var string
     */
    public $title = 'Контент на главной';

    public function __construct ( $data = [] ) {
        parent::__construct( $data );
        $this->setting = Setting::query()->firstOrCreate(['key' => 'content'], ['value' => []]);
    }

    /**
     * Handle the form request.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function handle ( Request $request ) {
        $this->setting->update($request->all());

        admin_success( 'Processed successfully.' );

        return redirect('/admin/settings?active=content');
    }

    /**
     * Build a form here.
     */
    public function form () {
        $this->embeds('value', __("Контент"), function (EmbeddedForm  $form) {
            $form->ckeditor('data', 'Контент на главной')->options(['lang' => 'ru', 'height' => 300]);
//            $form->textarea('data', 'Контент на главной')->options(['lang' => 'ru', 'height' => 300]);
        });
    }

    public function data()
    {
        return $this->setting->toArray();
    }
}
