<?php

use App\Admin\Controllers\OrderController;
use App\Admin\Controllers\UserController;
use Encore\Admin\Facades\Admin;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
    'as'            => config('admin.route.prefix') . '.',
], function (Router $router) {

    $router->get('/', 'HomeController@index');
    $router->resource('users', 'UserController');
  //  $router->resource('settings', 'SettingController');
    $router->resource('contacts', 'ContactController');
    $router->resource('questions', 'QuestionController');
    $router->resource('orders', 'OrderController');
    $router->get('/settings', 'SettingController@index')->name('settings');
    $router->resource('slider', 'SliderController')->except(['show', 'create']);

});


