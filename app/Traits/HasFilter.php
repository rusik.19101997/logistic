<?php

namespace App\Traits;

use App\Interfaces\ModelFilterInterface;
use Illuminate\Database\Eloquent\Builder;

/**
 * @method static Builder|static filter( \App\Interfaces\ModelFilterInterface $filter )
 */
trait HasFilter
{
    /**
     * @param $query
     * @param ModelFilterInterface $filter
     * @return Builder
     */
    public function scopeFilter ( $query, ModelFilterInterface $filter ): Builder {
        return $filter->apply( $query );
    }
}
