<?php

use App\Http\Controllers\ContactsController;
use App\Http\Controllers\MapPointsController;
use App\Http\Controllers\QuestionController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/confirm-code', [App\Http\Controllers\Auth\LoginController::class, 'confirmCode']);
Route::post('/confirm-code', [App\Http\Controllers\Auth\LoginController::class, 'confirmCodeAndLogin']);



Route::get('/contacts', [ContactsController::class, 'index'])->name('contacts');
Route::post('/questions', [QuestionController::class, 'store']);
Route::resource('/orders', \App\Http\Controllers\OrdersController::class);

Route::group(['middleware' => 'guest'], function () {
    Route::get('about', [\App\Http\Controllers\AboutController::class, 'index']);
});

Route::group(['prefix' => 'map'], function (){
    Route::get('/stores', [MapPointsController::class, 'stores']);
    Route::post('/allowed', [MapPointsController::class, 'getAllowedStore']);
});
