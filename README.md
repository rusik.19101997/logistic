# passing delivery

Проект попутная доставка

# Table of contents

- [Project Init](#project-init)
- [Admin Panel](#admin-panel)


# Admin Panel

```
Route: /admin
    Login: admin
    Password: qazwsx12
```

# Project Init

 ```bash
    docker compose up -d
    npm instal
    docker exec logistic_app php artisan migrate 
    docker exec logistic_app php artisan db:seed 
 ```
