<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {

            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('store_id');
            $table->json('place_to');
            $table->float('price');
            $table->date('delivery_date')->nullable();
            $table->date('shipping_date');
            $table->json('detail');
            $table->timestamps();
            $table->enum('status', array_keys(\App\Models\Order::STATUSES))->default(\App\Models\Order::STATUS_APPLICATION_COMPLETED);
            $table
                ->foreign("store_id")
                ->references("id")
                ->on("stores")
                ->onDelete("cascade");
            $table
                ->foreign("user_id")
                ->references("id")
                ->on("users")
                ->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
