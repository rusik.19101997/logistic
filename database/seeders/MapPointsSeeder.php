<?php

namespace Database\Seeders;

use App\Models\Store;
use App\Services\MapService;
use Illuminate\Database\Seeder;
use MStaack\LaravelPostgis\Geometries\LineString;
use MStaack\LaravelPostgis\Geometries\Point;
use MStaack\LaravelPostgis\Geometries\Polygon;

class MapPointsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run () {
        $points   = [];

        $polygons = [
           [ [ 44.6410, 40.6466 ], [ 44.5301, 40.6610 ], [ 44.4829, 40.6617 ], [ 44.4824, 40.6164 ], [ 44.4647, 40.6219 ], [ 44.4633, 40.7640 ], [ 44.3934, 40.7682 ], [ 44.1205, 40.7867 ], [ 44.3697, 41.5897 ], [ 44.4977, 41.9330 ], [ 44.6292, 42.0552 ], [ 45.0727, 42.1664 ], [ 45.4868, 42.0917 ], [ 45.8488, 41.7882 ], [ 45.8997, 41.5507 ], [ 45.9572, 41.3974 ], [ 45.9533, 41.1639 ], [ 46.1179, 41.0596 ], [ 46.1217, 40.9744 ], [ 46.3009, 40.9882 ], [ 46.2723, 40.4718 ], [ 45.8882, 40.0736 ], [ 45.7826, 40.1860 ], [ 45.4506, 40.5012 ], [ 45.3683, 40.1286 ], [ 44.8635, 40.0382 ], [ 44.6410, 40.6466 ] ],

            [ [ 43.3865, 40.0057 ], [ 43.4725, 40.0385 ], [ 43.5245, 39.9935 ], [ 43.6730, 40.2218 ], [ 43.6726, 40.2869 ], [ 43.7161, 40.2614 ], [ 43.7009, 40.2072 ], [ 43.6857, 40.0280 ], [ 43.6513, 39.8481 ], [ 43.7235, 39.6960 ], [ 43.7338, 39.6700 ], [ 43.7549, 39.6493 ], [ 43.8052, 39.6754 ], [ 43.7832, 39.6108 ], [ 43.8444, 39.5270 ], [ 43.9494, 39.3876 ], [ 44.0124, 39.3602 ], [ 44.0503, 39.1257 ], [ 43.3749, 39.9715 ], [ 43.3865, 40.0057 ] ],

            [ [ 44.0760, 39.0974 ], [ 44.0953, 39.0397 ], [ 44.1867, 38.8949 ], [ 44.1986, 38.8653 ], [ 44.2470, 38.8193 ], [ 44.3013, 38.7102 ], [ 44.3233, 38.6326 ], [ 44.3563, 38.5200 ], [ 44.3706, 38.3861 ], [ 44.4125, 38.2171 ], [ 44.4671, 38.1437 ], [ 44.5512, 38.0487 ], [ 44.5733, 37.9745 ], [ 44.6532, 37.6998 ], [ 44.6944, 37.4809 ], [ 44.7571, 37.3791 ], [ 44.8928, 37.2945 ], [ 44.9835, 37.2478 ], [ 45.1086, 36.8880 ], [ 45.1090, 36.7374 ], [ 45.1397, 36.6357 ], [ 45.1989, 36.5987 ], [ 45.2125, 36.6165 ], [ 45.2266, 36.6865 ], [ 45.2490, 36.8353 ], [ 45.3111, 36.7955 ], [ 45.3740, 36.7660 ], [ 45.4185, 36.7626 ], [ 45.4449, 36.7859 ], [ 45.4521, 36.8525 ], [ 45.4347, 36.9219 ], [ 45.3597, 37.0908 ], [ 45.3302, 37.2783 ], [ 45.3481, 37.3854 ], [ 45.6725, 37.6225 ], [ 45.7418, 37.7901 ], [ 46.0458, 37.9837 ], [ 46.1871, 38.2529 ], [ 46.3053, 38.2433 ], [ 46.3643, 38.0016 ], [ 46.4096, 37.8954 ], [ 46.5024, 37.8140 ], [ 46.6836, 37.7336 ], [ 46.7403, 38.2925 ], [ 46.8798, 38.4985 ], [ 46.8255, 38.7935 ], [ 46.8212, 38.8939 ], [ 46.7344, 38.8946 ], [ 46.7334, 38.8678 ], [ 46.5916, 38.8678 ], [ 46.5722, 38.8671 ], [ 46.5632, 38.9008 ], [ 46.6342, 38.9014 ], [ 46.6238, 39.0347 ], [ 46.5992, 39.0738 ], [ 46.6053, 39.1322 ], [ 46.6228, 39.2111 ], [ 46.6805, 39.2111 ], [ 46.6824, 39.2290 ], [ 46.7075, 39.2283 ], [ 46.7079, 39.2633 ], [ 46.7927, 39.2642 ], [ 46.7993, 39.4160 ], [ 46.8078, 39.4489 ], [ 46.8172, 39.4475 ], [ 46.8177, 39.4853 ], [ 46.8064, 39.4867 ], [ 46.8040, 39.5663 ], [ 46.7885, 39.5656 ], [ 46.7880, 39.6123 ], [ 46.7890, 39.8408 ], [ 46.7720, 39.8421 ], [ 46.7706, 40.0639 ], [ 46.7333, 40.0790 ], [ 46.7347, 40.1429 ], [ 46.6482, 40.1182 ], [ 46.6345, 40.0721 ], [ 46.5971, 40.0653 ], [ 46.5360, 40.0639 ], [ 46.5332, 40.2076 ], [ 46.4554, 40.2515 ], [ 46.4478, 40.2817 ], [ 46.3167, 40.2707 ], [ 46.3148, 40.3147 ], [ 46.2824, 40.3312 ], [ 46.2824, 40.7404 ], [ 46.1604, 40.3257 ], [ 46.0343, 39.9983 ], [ 45.8705, 39.9516 ], [ 45.7543, 39.9516 ], [ 45.6436, 39.9461 ], [ 45.5983, 40.0285 ], [ 45.4178, 40.0216 ], [ 45.3321, 40.0467 ], [ 45.2623, 40.0873 ], [ 45.2065, 40.0701 ], [ 45.1050, 40.0303 ], [ 45.0636, 39.9630 ], [ 45.0252, 39.8833 ], [ 45.0033, 39.8909 ], [ 45.0028, 39.9012 ], [ 44.9258, 39.9225 ], [ 44.9292, 39.9410 ], [ 44.8931, 39.9630 ], [ 44.8853, 39.9355 ], [ 44.8428, 39.9575 ], [ 44.7993, 39.9815 ], [ 44.7836, 39.9898 ], [ 44.7474, 40.0255 ], [ 44.7631, 40.0447 ], [ 44.7650, 40.0447 ], [ 44.7719, 40.0749 ], [ 44.7553, 40.0900 ], [ 44.7567, 40.0996 ], [ 44.7161, 40.1236 ], [ 44.7195, 40.1408 ], [ 44.7122, 40.1470 ], [ 44.7288, 40.2006 ], [ 44.7445, 40.1985 ], [ 44.7460, 40.2527 ], [ 44.7274, 40.2548 ], [ 44.7254, 40.3461 ], [ 44.7093, 40.3434 ], [ 44.7088, 40.3674 ], [ 44.7171, 40.3688 ], [ 44.7171, 40.4038 ], [ 44.7347, 40.4065 ], [ 44.7357, 40.4320 ], [ 44.7611, 40.4306 ], [ 44.7606, 40.5274 ], [ 44.7425, 40.5198 ], [ 44.7411, 40.5652 ], [ 44.6544, 40.5686 ], [ 44.6544, 40.6195 ], [ 44.6358, 40.6175 ], [ 44.6338, 40.6429 ], [ 44.5789, 40.6443 ], [ 44.5416, 40.6697 ], [ 44.4846, 40.6607 ], [ 44.4812, 40.6209 ], [ 44.4659, 40.6202 ], [ 44.4207, 40.4341 ], [ 44.3143, 40.4822 ], [ 44.3093, 40.4595 ], [ 44.2467, 40.3607 ], [ 44.1965, 40.3943 ], [ 44.1372, 40.4032 ], [ 44.1129, 40.4307 ], [ 44.0814, 40.1641 ], [ 44.0502, 40.1789 ], [ 43.9922, 40.1336 ], [ 44.0591, 39.7173 ], [ 44.0764, 39.3385 ], [ 44.0729, 39.1433 ], [ 44.0884, 39.1295 ], [ 44.0802, 39.1156 ], [ 44.0760, 39.0974 ] ]
        ];

        $store = Store::query()->firstOrCreate( [
            'latitude'  => 45.182218,
            'longitude' => 39.106907,
        ], ['name'      => 'Краснодарский склад'] );


        foreach ( $polygons as $key => $polygon ) {
            foreach ( $polygon as $point ) {
                $points[ $key ][] = new Point( $point[ 1 ],  $point[ 0 ] );
            }

            $store->polygons()->create(['area' => (new Polygon( [new LineString($points[ $key ])] ))->toWKT()]);
        }
    }
}
