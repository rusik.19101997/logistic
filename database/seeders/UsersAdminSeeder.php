<?php

namespace Database\Seeders;

use Encore\Admin\Auth\Database\Administrator;
use Encore\Admin\Auth\Database\Menu;
use Encore\Admin\Auth\Database\Permission;
use Encore\Admin\Auth\Database\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersAdminSeeder extends Seeder
{
    const ADMIN_INIT_LOGIN = 'admin';
    const ADMIN_INIT_PASSWORD = 'qazwsx12';

    public function run()
    {
        // create a user.
        Administrator::truncate();
        Administrator::create([
            'username' => self::ADMIN_INIT_LOGIN,
            'password' => Hash::make(self::ADMIN_INIT_PASSWORD),
            'name'     => 'Administrator',
        ]);

        // create a role.
        Role::truncate();
        Role::create([
            'name' => 'Administrator',
            'slug' => 'administrator',
        ]);

        // add role to user.
        Administrator::first()->roles()->save(Role::first());

        //create a permission
        Permission::truncate();
        Permission::insert([
            [
                'name'        => 'All permission',
                'slug'        => '*',
                'http_method' => '',
                'http_path'   => '*',
            ],
            [
                'name'        => 'Dashboard',
                'slug'        => 'dashboard',
                'http_method' => 'GET',
                'http_path'   => '/',
            ],
            [
                'name'        => 'Login',
                'slug'        => 'auth.login',
                'http_method' => '',
                'http_path'   => "/auth/login\r\n/auth/logout",
            ],
            [
                'name'        => 'User setting',
                'slug'        => 'auth.setting',
                'http_method' => 'GET,PUT',
                'http_path'   => '/auth/setting',
            ],
            [
                'name'        => 'Auth management',
                'slug'        => 'auth.management',
                'http_method' => '',
                'http_path'   => "/auth/roles\r\n/auth/permissions\r\n/auth/menu\r\n/auth/logs",
            ],
        ]);

        Role::first()->permissions()->save(Permission::first());

        // add default menus.
        Menu::truncate();
        Menu::insert([
            [
                'parent_id' => 0,
                'order'     => 1,
                'title'     => 'Dashboard',
                'icon'      => 'fa-bar-chart',
                'uri'       => '/',
            ],

            [
                'parent_id' => 0,
                'order'     => 2,
                'title'     => 'Admin',
                'icon'      => 'fa-tasks',
                'uri'       => '',
            ],
            [
                'parent_id' => 2,
                'order'     => 3,
                'title'     => 'Юзеры панели',
                'icon'      => 'fa-users',
                'uri'       => 'auth/users',
            ],
            [
                'parent_id' => 2,
                'order'     => 4,
                'title'     => 'Роли',
                'icon'      => 'fa-user',
                'uri'       => 'auth/roles',
            ],
            [
                'parent_id' => 2,
                'order'     => 5,
                'title'     => 'Доступы',
                'icon'      => 'fa-ban',
                'uri'       => 'auth/permissions',
            ],
            [
                'parent_id' => 2,
                'order'     => 6,
                'title'     => 'Меню',
                'icon'      => 'fa-bars',
                'uri'       => 'auth/menu',
            ],
            [
                'parent_id' => 2,
                'order'     => 7,
                'title'     => 'Логи операций',
                'icon'      => 'fa-history',
                'uri'       => 'auth/logs',
            ],
            [
                'parent_id' => 0,
                'order'     => 8,
                'title'     => 'Пользователи системы',
                'icon'      => 'fa-users',
                'uri'       => 'users',
            ],
            [
                'parent_id' => 0,
                'order'     => 9,
                'title'     => 'Контакты',
                'icon'      => 'fa-volume-control-phone',
                'uri'       => 'contacts',
            ],
            [
                'parent_id' => 0,
                'order'     => 10,
                'title'     => 'Вопросы',
                'icon'      => 'fa-question',
                'uri'       => 'questions',
            ],
            [
                'parent_id' => 0,
                'order'     => 11,
                'title'     => 'Заказы',
                'icon'      => 'fa-shopping-cart',
                'uri'       => 'orders',
            ],
            [
                'parent_id' => 0,
                'order'     => 12,
                'title'     => 'Контент',
                'icon'      => 'fa-pencil',
                'uri'       => 'settings',
            ],

        ]);

        // add role to menu.
        Menu::find(2)->roles()->save(Role::first());
    }
}
