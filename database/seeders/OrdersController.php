<?php

namespace Database\Seeders;

use App\Http\Controllers\Controller;
use App\Http\Filters\OrderFilter;
use App\Http\Requests\OrderStoreRequest;
use App\Models\Order;

class OrdersController extends Controller
{

    public function index(OrderFilter $filter)
    {
        $orders = Order::filter($filter)->where( 'user_id', auth()->user()->id )->get();

        return view( 'orders.error', [ 'orders' => $orders ] );
    }

    public function store ( OrderStoreRequest $request ) {
        $data              = $request->validated();
        $data[ 'user_id' ] = auth()->user()->id;

        if ( Order::query()->create( $data ) ) {
            return view( 'orders.success' );
        }

        return view( 'orders.error' );
    }
}
