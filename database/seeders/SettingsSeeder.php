<?php

namespace Database\Seeders;

use App\Models\Setting;
use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Setting::query()->firstOrCreate([ 'key' => 'contacts'],[
            'value' => [
                    'phone' => '88005758778',
                    'email' => 'contact@alidi.ru'
            ],
            'key' => 'contacts',
            'name' => 'Контакты',
        ]);
    }
}
