jQuery(document).ready(function($){
/* ------------------------------------------------------------------------------------------------------------------------------------ Sliders */

    $('.jq-eula-chbox').on('click', function () {
        if ( $(this).prop('checked') === true ) {
            $(this).parent().parent().parent().parent().find('.jq-eula-submit').removeClass('disabled');
        } else {
            $(this).parent().parent().parent().parent().find('.jq-eula-submit').addClass('disabled');
        }
    });





    $('.jq-input').focus(function() {
        $(this).parent().addClass('active');
    });
    $('.jq-input').blur(function() {
        if(!$(this).val()) {
            $(this).parent().removeClass('active');
        }
    });
/* ------------------------------------------------------------------------------------------------------------------------------------ phone mask */

    $('.jq-phone-mask').inputmask('+7 (999) 999-99-99');
    $('.jq-email-mask').inputmask("email");
    $('.jq-name-mask').bind("change keyup input click", function() {
        if (this.value.match(/[^а-яА-Я\s]/g)) {
            this.value = this.value.replace(/[^а-яА-Я\s]/g, '');
        }
    });
    $(document).on('click', '.js-modal', function() {
        $('#small-modal').arcticmodal();
    });
    $('.jq-pin-mask').inputmask('9 9 9 9');

/* ------------------------------------------------------------------------------------------------------------------------------------ Mobile menu */

    var menu_switcher = $('.js-menu-switcher');
    menu_switcher.on('click', function() {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).parent().removeClass('active');
            $(this).parent().addClass('passive');
            $('.js-site-mask').removeClass('active');
            $('#header').removeClass('active');
        } else {
            $(this).addClass('active');
            $(this).parent().addClass('active');
            $(this).parent().removeClass('passive');
            $('.js-site-mask').addClass('active');
            $('#header').addClass('active');
        }
    });
    $('.js-site-mask').on('click', function() {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $('.js-menu-switcher').removeClass('active');
            $('.js-menu-switcher').parent().removeClass('active');
            $('.js-menu-switcher').parent().addClass('passive');
            $('#header').removeClass('active');
        }
    });
    $('.js-menu-opener').on('click', function() {
        $(this).parent().toggleClass('opened');
        $(this).next().slideToggle();
    });

/* ------------------------------------------------------------------------------------------------------------------------------------ End */

});