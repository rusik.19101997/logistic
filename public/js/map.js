const mapHelper = {
    map: {},
    searchControl: {},
    addressText:'',

    searchWorker: function () {
        let mySearchControl = new ymaps.control.SearchControl({
            options: {
                position: {
                    top: -40
                },
                noPlacemark:true,
                zoomMargin: 120,
            },
        })


        let mySearchResults = new ymaps.GeoObjectCollection(null, {
            hintContentLayout: ymaps.templateLayoutFactory.createClass('$[properties.name]')
        });

        this.map.geoObjects.add(mySearchResults);

        mySearchControl.events.add('resultselect', function (e) {
            let index = e.get('index');
            mySearchControl.getResult(index).then(function (res) {
                let toImage = {
                    iconLayout: 'default#image',
                    // Своё изображение иконки метки.
                    iconImageHref: 'images/geo/to.svg',
                    // Размеры метки.
                    iconImageSize: [30, 42],
                    // Смещение левого верхнего угла иконки относительно
                    // её "ножки" (точки привязки).
                    iconImageOffset: [-5, -38]
                }
                mySearchResults.add(new ymaps.Placemark(res.geometry._coordinates, {
                    balloonContent: ''
                }, toImage));

                ymaps.geocode([  res.geometry._coordinates ]).then(  function (response) {
                    let addressText =   response.geoObjects.get(0).properties.get('text');
                    $('#map_to_coordinates').val(JSON.stringify({
                        coordinates: [ res.latitude, res.longitude ],
                        text: addressText
                    }));
                });

                mapHelper.setSearchResults(res.geometry._coordinates);
            });
        }).add('submit', function () {
            mySearchResults.removeAll();
        })

        mySearchControl.events.add('submit', function () {
           $('.map-container').addClass('loading');
        }, this);

        this.searchControl = mySearchControl
        this.map.controls.add(this.searchControl)

        let suggestView = new ymaps.SuggestView('map_to', {results: 1, })

        suggestView.events.add('select', function () {
            mapHelper.inputSearch()
        })

        document.getElementById('map_to').addEventListener("keyup", function (e) {
            if ( e.keyCode === 13 ) {
                mapHelper.inputSearch()
            }
        })

    },
    getAddress:  function (coords) {
         ymaps.geocode(coords).then(  function (res) {
            mapHelper.addressText =   res.geoObjects.get(0).properties.get('text');
        });

        return mapHelper.addressText;
    },
    inputSearch: function () {
        let request = document.getElementById('map_to').value
        this.searchControl.search(request);
        $('#map_from_coordinates').val('');
        $('#map_to_coordinates').val('');
    },
    setSearchResults: function (coords) {

        $.ajax({
            url: "/map/allowed",
            cache: true,
            data: { coordinates: coords },
            method: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("x-csrf-token", "{{ csrf_token() }}")
            },
            success: function (res) {
                let fromImage = {
                    iconLayout: 'default#image',
                    // Своё изображение иконки метки.
                    iconImageHref: 'images/geo/from.svg',
                    // Размеры метки.
                    iconImageSize: [30, 42],
                    // iconImageOffset: [-5, -38]
                }
                mapHelper.setObject([ res.latitude, res.longitude ], res.name, fromImage);

                ymaps.geocode([ res.latitude, res.longitude ]).then(  function (response) {
                    let addressText =   response.geoObjects.get(0).properties.get('text');
                    $('#map_from_coordinates').val(JSON.stringify({
                        coordinates: [ res.latitude, res.longitude ],
                        text: addressText
                    }));



                    $('#map_from').focus();
                    $('#map_from').val(addressText);
                    $('.map-container').removeClass('loading');
                });



                mapHelper.map.geoObjects
                    .add(new ymaps.Polyline([
                        [ res.latitude, res.longitude ],
                        coords
                    ], {
                        balloonContent: 'Расстояние',
                    }, {
                        strokeColor: "#1753FF",
                        strokeWidth: 2,
                        strokeOpacity: 0.5,
                        strokeStyle: '1 3'
                    }))

            },
            error: function (error) {
                // region1.events.add('click', function (e) { ... });
                if(error.status === 400) {
                    $('#map-not-found-modal').show();
                }
            }
        })
    },
    setPolygons: function (polygons) {
        let map = this.map;

        polygons.forEach(function (item, index) {
            map.geoObjects.add(new ymaps.Polygon(item, {
                hintContent: "Polygon"
            }, {
                fillColor: '#00FF0088',
                strokeWidth: 1
            }))
        })
    },

    setObject: function (coords, title, icon = { preset: 'islands#icon', iconColor: '#0095b6' }) {
        this.map.geoObjects
            .add(new ymaps.Placemark(coords, {
                balloonContent: title
            }, icon))
    },
    mapInit: function (block) {
        this.map = new ymaps.Map(block, {
            center: [ 47.2357, 39.7015 ],
            zoom: 5,
            controls: []
        }, {
            suppressMapOpenBlock: true
        })
    },
    setCurrentLocation: function () {
        let map = this.map
        ymaps.geolocation.get().then(function (res) {
            map.setCenter(res.geoObjects.get(0).properties.get('boundedBy')[ 0 ], 5)
        })
    }, handle: function (block  = 'map') {
        this.mapInit(block)
        // this.setCurrentLocation()
        this.searchWorker()
    }
}
$('#map_to').on('keyup', function (){
    $('#map_from').val('');
});


const mapWorker = {
    forFirstPage: function () {
        ymaps.ready(init)

        function init() {
            mapHelper.handle()
        }

        $('#first-btn-submit').on('click', function (){
            if( $('#map_to_coordinates').val().length > 2 && $('#map_from_coordinates').val().length )  {
                $('#first-map-form').submit();
            }
        });
    }
};


mapWorker.forFirstPage();


