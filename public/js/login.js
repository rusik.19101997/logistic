$('#resend-sms').click(function (e) {
    e.preventDefault()
    let userId = document.getElementById('confirm-user_id').value

    $.ajax({
        url: "/api/resend-code",
        data: { user_id: userId },
        method: 'POST',
        beforeSend: function (xhr) {
            xhr.setRequestHeader("x-csrf-token", "{{ csrf_token() }}")
        },
        success: function (res) {
            document.getElementById("expect-seconds").innerHTML = res.data
            timer(res.data)

        },
        error: function () {
            timer(1)
        }
    })
})

function timer(seconds) {
    $('#for-resend').addClass('d-none')
    $('#expect-for-send').removeClass('d-none')

    let x = setInterval(function () {
        document.getElementById("expect-seconds").innerHTML = seconds
        seconds -= 1

        if ( seconds < 0 ) {
            $('#for-resend').removeClass('d-none')
            $('#expect-for-send').addClass('d-none')

            clearInterval(x)
        }
    }, 1000)
}
