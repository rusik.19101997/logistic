require('./bootstrap')
function ajax(url, body = {}, callback, method = 'GET') {
    let http = new XMLHttpRequest()
    http.open(method, url, true)

    http.setRequestHeader("x-csrf-token", "{{ csrf_token() }}");
    http.setRequestHeader("Accept", "application/json");
    http.setRequestHeader("Content-Type", "application/json; charset=utf-8");

    http.onreadystatechange = callback;
    http.send(JSON.stringify(body))
}
