@extends('layouts.app')

@section('content')
    <section id="page" class="inner-page page-404">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="content-404">
                        <img src="/img/404-img.png" alt="Страница не найдена">
                        <h1>Страница не найдена</h1>
                        <p>Вероятно, страница не существует или в адресной строке браузера какая-то ошибка. Вы можете
                            вернуться на главную страницу и продолжить работу.</p>
                        <div class="link-wrap">
                            <a href="/" title="Главная">Главная</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
