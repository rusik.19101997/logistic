<div id="promo" class="">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-7">
                <h1>Междугородняя доставка<br> <span class="black">коммерческих грузов</span> <span class="price">от 200 ₽</span></h1>
            </div>
            <div class="col-12 col-lg-5">
                <div class="request-form">
                    <div class="title">Оформление заявки</div>
                    <label for="where" class="input-label">
                        <span class="txt">Куда</span>
                        <input type="text" id="map_to" class="input jq-input" placeholder="">

                    </label>
                    <label for="from" class="input-label">
                        <span class="txt">Откуда</span>
                        <input type="text" id="map_from" class="input jq-input" placeholder="">
                    </label>

                    <form id="first-map-form" action="/orders/create">
                        <input type="hidden" id="map_from_coordinates"  name="from" required>
                        <input type="hidden" id="map_to_coordinates" name="to" required>
                    </form>

                    <button id="first-btn-submit" class="submit site-btn">Перейти к оформлению</button>
                    @if(Session::has('first-step-error'))
                        <div class="invalid-feedback d-block">
                            {{ Session::get('first-step-error') }}
                        </div>
                    @endif

                    <div id="map"  class="map-container map js-map"></div>
                </div>

            </div>
        </div>
    </div>
</div>

@section('scripts')
    <script src="https://api-maps.yandex.ru/2.1/?apikey=e64eaa4d-c4c8-418a-bea6-c092fc2b47c7&lang=ru_RU" type="text/javascript"></script>
    <script src="js/map.js?asd=623"></script>
@endsection
