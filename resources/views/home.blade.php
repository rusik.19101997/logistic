@extends('layouts.app')


@section('content')
    <x-first-order-form/>
    {!! $content !!}



    <section id="banners">
        <div class="container">
            <div class="row">
                @php
                $key = 1;
                @endphp
                @foreach ($main_banners as $item)
                    <div class="col-12 col-sm-6 col-lg-6">
                        <div class="info-block block-{{$key}}" style="background-image: url({{$item['image']}});">
                            <div class="title">{{$item['title']}}</div>
                            <p>{{$item['text']}}</p>
                            <div class="tags">
                                <span>{{$item['time']}}</span>
                                <span>{{$item['price']}}</span>
                            </div>
                        </div>
                    </div>
                    @php
                        $key++;
                    @endphp
                @endforeach


            </div>
        </div>
    </section>
@endsection
