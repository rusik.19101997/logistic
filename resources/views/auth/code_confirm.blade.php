@php
    $cache_time = 0;
    if( $cache = Cache::get("resend_code_{$user_id}")) {
      $cache_time = now()->diffInSeconds($cache);
    }
@endphp

@extends('layouts.app')

@section('content')
    <section id="page" class="inner-page">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <form class="login-form" method="POST">
                        <input type="hidden" id="confirm-user_id" name="user_id" value="{{$user_id}}">
                        <a href="#" class="back-btn">Назад</a>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-12">
                                    <div class="text-center">
                                        <h2>{{session()->get('has_order') ? "Для продолжения оформления требуется авторизация"  : 'Авторизация'}}</h2>
                                    </div>
                                    <div class="input-wrap">
                                        <div class="data">
                                            <input class="input jq-pin-mask" name="code" type="text"
                                                   placeholder="_ _ _ _">
                                            <span class="info-text">Введите код</span>
                                            <button class="submit">Отправить</button>
                                        </div>
                                    </div>
                                    @error('code')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    <div id="expect-for-send" class="reminder {{$cache_time > 0 ? '' :  'd-none'}}">
                                        <p>Повторная отправка через: <span id="expect-seconds">{{$cache_time}}</span>
                                        </p>
                                    </div>
                                    <div class="reminder {{$cache_time > 0 ? 'd-none' : ''}}" id="for-resend">
                                        <p>Не пришел код? <a href="#" id="resend-sms">Отправить еще раз</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    {{--

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">{{ __('Confirm Code') }}</div>

                        <div class="card-body">
                            <form method="POST" action="/confirm-code">
                                @csrf

                                <div class="row mb-3">
                                    <label for="email" class="col-md-4 col-form-label text-md-end">{{ __('Code') }}</label>

                                    <div class="col-md-6">
                                        <input id="code" type="text" class="form-control @error('code') is-invalid @enderror" name="code"  required  autofocus>
                                        <input type="hidden" id="confirm-user_id" name="user_id" value="{{$user_id}}">
                                        @error('code')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>



                                <div class="row mb-0">
                                    <div class="col-md-8 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Confirm') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                            <form method="POST" action="/resend-code">
                                @csrf
                                <input type="hidden" name="user_id" value="{{$user_id}}">
                                <div class="row mt-2">
                                    <div class="col-md-8 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Resend') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>--}}
@endsection


@section('scripts')
    <script src="js/login.js?vet=12"></script>

    @if($cache_time > 0)
        <script>
            timer({{$cache_time}})
        </script>
    @endif
@endsection
