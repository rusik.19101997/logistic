@extends('layouts.app')
@section('content')
    <section id="page" class="inner-page">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <form class="login-form"  method="POST">
                        <a href="{{url('/')}}" class="back-btn">Назад</a>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-12">
                                    <div class="text-center">
                                        <h2>{{session()->get('has_order') ? "Для продолжения оформления требуется авторизация"  : 'Авторизация'}}</h2>
                                    </div>
                                    <div class="input-wrap">
                                        <div class="data phone">
                                            <input id="phone" type="text"
                                                   class=" input jq-phone-mask" name="phone"
                                                   value="{{ old('phone') }}" required autocomplete="email" autofocus  placeholder="+7 (___) ___-__-__">
                                            <span class="info-text">Ваш номер телефона</span>
                                            <button class="submit disabled jq-eula-submit">Отправить код</button>
                                        </div>
                                    </div>
                                    <div
                                        class="flex-row align-content-center justify-content-center d-flex invalid-feedback">
                                        @error('phone')
                                        <div
                                            class="flex-row align-content-center justify-content-center d-flex invalid-feedback">

                                            <strong>{{ $message }}</strong>
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="reminder">
                                        <div class="eula-wrap">
                                            <label>
                                                <input type="checkbox" name="chbox" class="checkbox jq-eula-chbox">
                                                <span class="ch-custom"></span>
                                                Я даю согласие на обработку персональных даных
                                            </label>
                                        </div>
                                    </div
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
