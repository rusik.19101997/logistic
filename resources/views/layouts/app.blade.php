<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon/android-icon-96x96.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/favicon/android-icon-192x192.png">
    <link rel="manifest" href="{{ url('/favicon/manifest.json') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <link rel='stylesheet' href='/includes/bootstrap/bootstrap.css' type='text/css' media='all' />
    <link rel='stylesheet' href='/includes/bootstrap/bs-fix.css' type='text/css' media='all' />
    <link rel='stylesheet' href='/includes/slick/slick.css' type='text/css' media='all' />
    <link rel='stylesheet' href='/includes/jquery.arcticmodal.css' type='text/css' media='all' />
    <link rel='stylesheet' href='/includes/style.css' type='text/css' media='all' />
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">


    <title>Alidi</title>
</head>
<body>
<div class="site-mask js-site-mask"></div>
<header id="header">
    <div class="container">
        <div class="row">
            <div class="col-4 col-lg-6 align-self-center">
                <div class="site-menu">
                    <div class="logo"><a href="/"><img src="/img/logo-header.svg" alt="Alidi"></a></div>
                    <div class="menu-ico js-menu-switcher">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                    <div class="menu-content">
                        <div class="top">
                            <ul class="main-menu">
                                <li><a href="#">Оформление</a></li>
                                <li><a href="#">Сервис</a></li>
                                <li><a href="#">Контакты</a></li>
                            </ul>
                        </div>
                        <div class="bottom">
                            <div class="title js-menu-opener">Другие направления АЛИДИ</div>
                            <div class="otherways">
                                <a class="dist" href="https://alidi.ru/catalog">Дистрибуция</a>
                                <a class="logst" href="https://alidi-logistics.ru/">Логистика</a>
                                <a class="online" href="https://alidi.ru/">Онлайн-платформа</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-8 col-lg-6 align-self-center">
                <div class="row">
                    <div class="col-8 col-lg-10 align-self-center">
                        <div class="phone"><a href="tel:{{preg_replace( '/[^\d]/', '', $contacts_data['phone'] )}}">{{$contacts_data['phone']}}</a> <span> — {{$contacts_data['schedule']}}</span></div>
                    </div>
                    <div class="col-4 col-lg-2 align-self-center text-right">
                        @guest
                            @if (Route::has('login'))
                                <a class="account" href="{{ route('login') }}"></a>
                            @endif
                        @else
                            <a class="account" href="#" title="Иванов И.И.">
                                <div class="name">{{ Auth::user()->name }}</div>
                                <div class=""></div>
                            </a>
                        @endguest

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="mobile-menu-content"><!-- Мобильное меню -->
        <div class="top">
            <ul class="main-menu">
                <li><a href="#">Оформление</a></li>
                <li><a href="#">Сервис</a></li>
                <li><a href="#">Контакты</a></li>
            </ul>
        </div>
        <div class="bottom">
            <div class="title js-menu-opener">Другие направления АЛИДИ</div>
            <div class="otherways">
                <a class="dist" href="https://alidi.ru/catalog">Дистрибуция</a>
                <a class="logst" href="https://alidi-logistics.ru/">Логистика</a>
                <a class="online" href="https://alidi.ru/">Онлайн-платформа</a>
            </div>
        </div>
    </div>
</header>
<div class="pop-ups">
    <div class="box-modal back-call" id="small-modal" data-modal="back-call">
        <div class="modal-close arcticmodal-close"></div>
        <div class="popup-form">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 text-center">
                        <img class="modal-ico" src="/img/modal-ok-ico.svg" alt="Cпасибо за заявку">
                        <h2>Cпасибо за заявку</h2>
                        <p>Мы свяжемся с вами в ближайшее время.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 text-center">
                        <div class="arcticmodal-close arcticmodal-close-bottom">Закрыть</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="app">
    <main >
        @yield('content')
    </main>
    <!-- Scripts -->

</div>


<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-6 align-self-center">
                <div class="left-side">
                    <div class="row">
                        <div class="col-12 col-lg-6 col-xl-3 align-self-center">
                            <div class="logo">
                                <img src="/img/footer-logo.png" alt="Alidi - Попутная доставка">
                            </div>
                        </div>
                        <div class="col-12 col-lg-6 col-xl-9 align-self-center">
                            <ul class="footer-menu">
                                <li><a href="#">Оформление</a></li>
                                <li><a href="#">Сервис</a></li>
                                <li><a href="{{route('contacts')}}">Контакты</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-6 align-self-center right-side">
                <div class="contacts-wrap">
                    <div class="cont-info">
                        <div class="title">Бесплатный звонок по России</div>
                        <div class="data"><a href="tel:{{preg_replace( '/[^\d]/', '', $contacts_data['phone'] )}}">{{$contacts_data['phone']}}</a> — {{$contacts_data['schedule']}}</div>
                    </div>
                    <div class="cont-info">
                        <div class="title">Email:</div>
                        <div class="data"><a href="mailto:{{$contacts_data['email']}}">{{$contacts_data['email']}}</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
</body>
<script type='text/javascript' src='/includes/jquery.3.3.1.min.js'></script>
<script type='text/javascript' src='/includes/bootstrap/bootstrap.min.js'></script>
<script type='text/javascript' src='/includes/slick/slick.js'></script>

<script type='text/javascript' src='/includes/jquery.inputmask.min.js'></script>
<script type='text/javascript' src='/includes/jquery.arcticmodal.js'></script>
<script type='text/javascript' src='/jq.ready.js'></script>

<script src="{{ asset('js/app.js') }}" defer></script>

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
@yield('scripts')
</html>
